<link rel="shortcut icon" href="../img/php.ico" />
<?php
/**
 * Make a class iterable, that is mean the ability to loop through it
 * and in each loop access to the array element set with getIterator() return
 */

// 1-make an object traversable by making the class implements the  IteratorAggregate interface:
class node  implements IteratorAggregate
{
    // 2-private $tech array cannot be accessed outside of the class.
    private $tech;

    //3-fill the array using the constructor
    public function __construct() {
        $this->tech = explode( ',', 'PHP,HTML,XHTML,CSS,JavaScript,XML,XSLT,ASP,C#,Ruby,Python') ;
    }




    //4-when we use a foreach on this class(collection object) this will allow us to iterate over the items passed to new ArrayIterator( );
    public function getIterator() {
        // 5-return iterator (return something which can be iterate)
        return new ArrayIterator( $this->tech );
    }

}


// create object
$node_object = new nodes();


// iterate over collection
foreach ($node_object  as $key => $element_value) {
    echo "<p>Technology $key : $element_value</p>";
}