<?php
include_once  'Product.php';
include_once  'CartItem.php';
//https://www.youtube.com/watch?v=cDpgNUq4KBs
class  ClothingProduct extends  Product{
    //2.inherit the CartItem property
    use CartItem;
    public function getProducts(){
        //3.Calling the class that has been inherited with trait
      return $this->getCartItems();
    }
}

$product = new ClothingProduct();
var_dump($product->getProducts());