<?php
/**
 *  Encapsulation is hidden the class member form an outside class
 * Implementing Encapsulation though step 1 and 2
 */
class Employee{
    //1. private data variable : so that these vars can not accessed directly from outside the class
    private $gender;

    //2. getter and setter methods : to set and get the values if the fields.
    public function setGender($gender){
        if($gender !== 'male' || $gender !== 'femal'){
            throw new Exception('Please set male of femal for gender !!!!');
        }
        $this->gender = $gender;
    }

}

$emp1 = new Employee();
echo "<h1>before exception</h1>";
$emp1->setGender('malsdsdsd');
echo "<h1>test</h1>";
