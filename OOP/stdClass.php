<?PHP
/**
 * The stdClass is the empty class in PHP which is used to cast other types to object
 * It is useful in dynamic object.
 */

// convert an array into StdClass instance
// each key in the array will be a public property of StdClass instance
$friends_arr= ['john' , 'petter', 'Brown'];
$friends_obj = (object) $friends_arr; //// type casting from array to object

var_dump($friends_obj);




// Object-styled definition of an employee
$employee_object = new stdClass;
$employee_object->name = "John Doe";
$employee_object->position = "Software Engineer";
$employee_object->address = "53, nth street, city";
$employee_object->status = "Best";

// Display the employee contents
print_r($employee_object);