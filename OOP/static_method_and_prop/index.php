<?php

/**
 * Describe static properties and methods.
 *
 * Static property :  is a property that is gonna be shared by
 * all or some of the class instances.
 *
 */
class DB {

  // Static properties are properties that gonna be shared
  // by all objects or part of them.
  public static $type = 'MySql';

  public static function setDbTyp($type){
    self::$type =  $type;
  }
  // static property can be access from withing a non static methods.
  public function connect(){
    return self::$type;
  }

}

// Set the global property once.
db::setDbTyp('PostgreSQL');

//make all other objects use that property.
$db = new DB();
echo $db->connect().'<br/ >';

$db2 = new DB();
echo $db2->connect().'<br/ >';;