<?php
// Create a subClass of Exception class
// so that it will inherit all parent methods.
class AgeException extends Exception{

  // using constructor to set default exception message.
  public function __construct($age ) {
    $this->message = "Exception:  <b>{$age}</b> is to young, it must be > 18";
  }

  public function info(){

   $info =  <<<ETD
   <div style="background-color: indianred; padding: 10px; border: 2px solid;">
       Exception in file : <b>{$this->file}</b>  , at line: <b>$this->line</b> <br />
       with a message : {$this->message} <br />
   </div>
ETD;

   return $info;
  }
}