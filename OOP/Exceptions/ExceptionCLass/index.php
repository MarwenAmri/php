<?php
// include the new created exception Sub lass
include "AgeException.php";

$age = 16;

//Try a piece of code
try{

  // and when and Exception thrown , throw exception using the
  // created exception subCLass which $can be used exactly like the
  //Exception class +
  if($age < 18){
    throw new AgeException($age);
  }

}

  //display an the expction message + other infos using the catch block
catch(AgeException $ageException){
  echo "Message : ".$ageException->getMessage();
  echo "info :".$ageException->info();
}
