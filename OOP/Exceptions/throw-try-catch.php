<?php

$age = 16;
//Try a piece of code
try{
    // and when and Exception thrown , set teh exception code
    if($age < 18){
        throw new Exception('not older enough !!!!!');
    }

}

//display an the expction message + other infos using the catch block
catch(Exception $exception){
    echo "Message : " .$exception->getMessage();

    echo "<br />";
    echo "File: " .$exception->getFile();

    echo "<br />";
    echo "Line: " .$exception->getLine();

    echo "<br />";
    echo "Line: " .$exception->getTraceAsString();
}


//dont stop the eduction and continue runing
echo "<h1>don't stop the script execution , but continue the execution after the cached Exception</h1>";

//catch another exception
try{
  if (!file_exists('t.php')){
    throw new Exception(" trying to get a non existing  file");
  }
}catch (Exception $e){
  echo 'File Exception : ' . $e->getMessage();
}

//continue
echo "<h1>continue the execution after catching 2nd exception</h1>";