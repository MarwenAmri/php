<?php
include_once 'class/Car.php';

$car = new Car();

// when user try to print an object
// __toString()  : method will be called
echo $car;
echo "<br />";

//when user try to print an non existing class property
// __get($propName) : method will be called
echo $car->age;
echo "<br />";

// when a user try to set a non exiting property
// so __set() method will be executed auto.
$car->age = 100;
echo "<br />";


// when a user try to call a non existing method
// so the __call() method will executed auto.
$car->SomethingThatDoesntExist(1,2,3);
echo "<br />";

// Store a copy of an object  in variable and not
//  and don't store a reference to the original one
$car2 = clone $car;
echo "<br />";


// the __destruct will be called auto when user finish using the object
// or when he use unset($car);

