<?php
/**
 * Magic methods are a method that executed automatically by php
 * when a specif event occurred
 *
 */


class Car {
  public $name = "Jane";

  /**
   * The __toString() method allows a class to decide how it will react when
   * it is treated like a string.
   *
   * For example, what echo $obj; will print. This method must return a string,
   * as otherwise a fatal E_RECOVERABLE_ERROR level error is emitted.
   *
   * echo $obj;
   */
  public function __toString() {
   return 'this the Car class';
  }


  /**
   * when user try to get a property that doesn't exist
   * this will be anatomically called.
   *
   * echo $obj->prop;
   */
  public function __get($propName) {
    echo "you try to access to a property that doesn't exist : <b>{$propName}</b>";
  }

  /**
   * when user try to set a property that doesn't exist
   * this will be anatomically called.
   *
   * $obj->prop = $value;
   */
  public function __set($propName , $value){
  echo "you are trying to set a non exisit property  <b>{$propName}</b> with value {$value} ";
  }

  /**
   * when user try to call a method that doesn't exist
   * this will be anatomically called.
   *
   * $obj->prop = $value;
   */
  public function __call($methodName, $params) {
     echo "you are going to process a non existing method  $methodName, ";
     echo "with this parameters :"; print_r($params);
  }

  /**
   * called  when the object is destructed using : unset($obj)
   * or the script is stopped or exited.
   *
   * If you create a __destruct() function, PHP will automatically
   * call this function at the end of the script.
   *
   * unset($car);
   * https://www.youtube.com/watch?v=5O_EP5op7k8
   */
  function __destruct() {
    echo "<br /><br /> object has been deleted ... end";
  }

  /**
   * when a user try to clone an object
   * call the __clone method
   *
   * $obj2 = clone $obj;
   */
  function __clone() {
    echo 1;
  }


}