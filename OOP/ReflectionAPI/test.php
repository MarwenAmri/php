<?php
use \Ref\Controller\User;
// a reflection API enable us to examine and modify the behavior of
// class or method oe an interface
// ex: make protected method accesible
include_once 'class/User.php';

// 1-create a RefelctionMethod object  for the method that you want examne or mofidy behavior
$ReflectionMethod =  new \ReflectionMethod(User::class,'add');

// 2-make the add private method public to be called in nexte step using the relection method
$ReflectionMethod->setAccessible(true);

// 3-invoking the add(1,2,3) method
echo $ReflectionMethod->invokeArgs(new User() ,[10,20,30]);
