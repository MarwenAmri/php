<?php
include_once 'class/User.php';
use Ref\Controller\User;
echo "<h1> -----------------------  ReflectionClass ----------------- </h1>";
/**
 * a Reflection class enable us to inspect a specific class :
 *  - get class properties , methods and constants
 *  - get a class, method or property docComment
 *  - get a class full path
 */
$rc = new ReflectionClass('Ref\Controller\User');

//print all methods that this reflexion class offer
get_class_methods($rc)  ;

// get the 'User' class full path
var_dump( $rc->getFileName());
var_dump($rc->getName());


// return all class methods and properties (public + private + protected) of 'User' class
var_dump( $rc->getMethods());
var_dump( $rc->getProperties());
var_dump($rc->getConstants());

// Getting a spefic property/method scope (public , private , public or static)
// argument : ReflectionProperty::IS_STATIC  , ReflectionProperty::IS_PUBLIC , ReflectionProperty::IS_PROTECTED , ReflectionProperty::IS_PRIVATE
echo "<h2> public  methods</h2>";
var_dump( $rc->getMethods(ReflectionProperty::IS_PUBLIC ) );

echo "<h2> private methods</h2>";
var_dump( $rc->getMethods(ReflectionProperty::IS_PRIVATE ) );

echo "<h2> public  properties</h2>";
var_dump( $rc->getProperties(ReflectionProperty::IS_PUBLIC ) );
var_dump( $rc->getProperties(ReflectionProperty::IS_PRIVATE) );

// checheck if class has proeprty of a method
echo "<h2>check if a class has property/method</h2>";
var_dump( $rc->hasMethod('getData'));
var_dump($rc->hasProperty('data'));

// Getting class n method and properties docComment
echo "<h2> Get a class DocComment</h2>";
echo $rc->getDocComment().'<br />';

####################################################################
######################### ReflectionMetho ##########################
####################################################################
echo "<h1> ----------------------- ReflectionMethod ----------------- </h1>";

$rm = new ReflectionMethod('Ref\Controller\User','getData');

echo "<h2> Get a method DocComment</h2>";
echo  $rm->getDocComment() ;

echo "<h2> check if methods public or private or protected :</h2>";
echo $rm->isPublic();

//make a private function publics
echo "<h2> make private or protected  function public</h2>";
//1.create the ReflectionMethod Class for the private   method
$privateMeReflectionMethod = new \ReflectionMethod(User::class,'add');
//2.use the RefectionMethod class method to make the method accessible to be invoked by the Reflection method in next step
$privateMeReflectionMethod->setAccessible(true);
//3.invoking a method with the proper paremters
$userObj = new \Ref\Controller\User();
$args = [2,1,10];
echo $privateMeReflectionMethod->invokeArgs($userObj,$args );//ReflectionMethod::invokeArgs ( object $object , array $args )


####################################################################
######################### ReflectionProperty ##########################
####################################################################
echo "<h1> ----------------------- ReflectionMethod ----------------- </h1>";

echo "<h2> Get a property DocComment</h2>";
$rp = new ReflectionProperty('Ref\Controller\User','data');
echo  $rp->getDocComment() ;
