<?php
include_once "classes/Car.php";
include_once "classes/Parking.php";


//1.Create an object , set its attributes , and execute some operations
$bmw = new Car();
    $bmw->speed = 260;
    $bmw->engine = '4 cylinder ';
    $bmw->setModel('X5');

//2.then pass that built object into another class method
$parking = new Parking();
    //DI: injecting already made objects into our class through the constructor
    //instead of making our class (Parking) create the object itself internally
    // to separate the concern so that the only concern of our class
    //is just to use that already made object and not to create it
    $parking->carInfo($bmw );