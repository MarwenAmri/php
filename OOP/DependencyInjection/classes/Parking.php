<?php

/**
 * Class Parking
 * our class will only use the already made object
 * to reduce dependency.
 */
class Parking{
    function carInfo(Car $car){
        //3.Access to an object predefined operations and attributes
        echo $car->speed."<br />";
        echo $car->getModel();

    }
}