<?php
/**
 * use a CALLBACK when you want to enable an external intervention , at specific point during function run time lifecycle.
 * this enable other developers to extend and alter our function , without changing the original code.
 */

//Object : alter argument by callback before being processed.

//3- The intervention : The callback implementation will process the Arguments .
function calback_get_user($user){
    $user = ['name' => $user];
    return  $user;
}


function call($callback , $user){
    //2-  External Intervention point : The called function receive the callback and its arguments  , instead of directly process it ,
    // it will do some intervention at this point (call the callback function and pass argument to it).
    $user =  $callback($user);

    //4 - Continue after the intervention
    return   $user ;
}

//1- Trigger the function  : call the function that will invoke the callback function set in arg.
var_dump( call('calback_get_user','ali') );